/* Initialisation variables */
let tl = [ 421, "€"];
document.getElementById("total").innerHTML = tl[0] + " "+ tl[1] ;
console.log(tl);
let debit = [1098.99, "€"];
document.getElementById("totalDebit").innerHTML = debit[0] + debit[1];
console.log(debit);
let pdebit = [72.29, "%"];
document.getElementById("totalDebitPercent").innerHTML = pdebit[0] + pdebit[1];
console.log(pdebit);
let credit = [1520, "€"];
document.getElementById("totalCredit").innerHTML = credit[0] + credit[1];
const ope = document.querySelector("#operation");
const ulc = document.getElementById("cred");
const uld = document.getElementById("deb");

  /* fonction onclick */
function myFunction() {
    /* recuperation intitulé et montant */
    const montant = document.querySelector("#montant");
    const intitule = document.querySelector("#intitule");
    /* recup donnée dans varibles pour calculs.  */
    const c = Number(credit[0]);/* conversion char en number */
    const m = Number(montant.value);
    const d= Number(debit[0]);
    console.log(montant.value);
    /* s'il n'y a pas de montant alors */
    if (montant.value.length == "0"){
        alert("retry again");
    } else{
    if (ope.value == "+") {  /* si opérateur est positif = crédit */
        /* création ligne de tableaux */
        const li = document.createElement('li');
        const span = document.createElement('span');
        const span2 = document.createElement('span');
        /* rattachement de la nouvelle ligne dans le tableaux et rattachement des span dans la ligne */
        ulc.appendChild(li);
        li.appendChild(span2);
        li.appendChild(span);
/* attribution des classes de style correspondants */
        span.setAttribute("class", "span_New");
        span.classList.add("montant", "txt-color-gazoil");
        span2.classList.add("intitule");
        /* actualisation du crédit */
        credit[0] = Number(credit[0]) + Number(montant.value);
        /* afficher nouveaux calcul a l'écran */
        document.getElementById("totalCredit").innerHTML = credit[0] + credit[1];
        /* actualisation total */
        tl[0] = Number(credit[0]) - Number(debit[0]);
         /* afficher nouveaux calcul a l'écran */
        document.getElementById("total").innerHTML = tl[0] + tl[1] ;
        /*  mise dans les span et tableaux des nouvelles variables */
        span.innerHTML = montant.value + "€";
        span2.innerHTML = intitule.value;
        li[0] = span2;
        li[1] = span;
        /* calcul du pourcentage de totaldebitpercent */
        const per=(d/c)*100;
         /* actualisation totaldebitpercent */
    document.getElementById("totalDebitPercent").innerHTML=Number(per).toFixed(2);
    } else {
        /* création ligne de tableaux */
        const li = document.createElement('li');
        const span3 = document.createElement('span');
        const span4 = document.createElement('span');
        const span5 = document.createElement('span');
         /* rattachement de la nouvelle ligne dans le tableaux et rattachement des span dans la ligne */
        uld.appendChild(li);
        li.appendChild(span3);
        li.appendChild(span4);
        li.appendChild(span5);
        /* attribution des classes de style correspondants */
        span4.setAttribute("class", "span_New");
        span4.classList.add("montant", "txt-color-red");
        span3.classList.add("intitule");
        span5.classList.add("percent", "txt-color-red");
        /* actualisation debit */
        debit[0] = Number(debit[0]) + Number(montant.value);
        /* cacul a l ecran */
        document.getElementById("totalDebit").innerHTML = debit[0] + debit[1];
        /*  actualisation total */
        tl[0] = Number(credit[0]) - Number(debit[0]);
        document.getElementById("total").innerHTML = tl[0].toFixed(2) + tl[1] ;
        /*  calcul pourcentage d'un débit */
        const pourc = (m / c) * 100;
        /* affichage a l'écran + arrondi 2 chiffre apres virgule */
        span5.innerHTML = pourc.toFixed(2) + "%";
        span4.innerHTML = montant.value + "€";
        span3.innerHTML = intitule.value;
        li[0] = span3;
        li[1] = span4;
        li[2] = span5;
        const per=(d/c)*100;
    document.getElementById("totalDebitPercent").innerHTML=Number(per);
    }
}
};